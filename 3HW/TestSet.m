% Ethan Petuchowski     Data Mining HW#3

% ==============================================================
% ================== INITIALIZATIONS ===========================

load('hw3_netflix.mat')
tau = 30;
k = 10;
lambdaSet = 1;  % 0.05:0.05:1;
numLambdas = length(lambdaSet)
numValidations = 1;
resultMatrix1 = zeros(numLambdas,numValidations);

% ==============================================================
% ================= RUN EM and PRODUCE RMSEs ===================

% lambda is just '1' in this file
for lambda = lambdaSet
    U = unidrnd(5, length(Ratings(:,1)), k);    % generate random U,M matrices
    M = unidrnd(5, length(Ratings(1,:)), k);

    %%%% THE EM %%%%
    for iteration = 1:tau
        % update M
        for j = 1:length(M(:,1))
            % find U(Kj) & r_j(Kj), they correspond to nonzero ROWS of trR(:,j)
            [rows,cols,rkj] = find(trR(:,j));
            Ukj = U(rows,:);
            M(j,:) = ((Ukj'*Ukj)+lambda*eye(k))\(Ukj'*rkj);
        end

        % update N
        for i = 1:length(U(:,1))
            [rows,cols,rki] = find(trR(i,:));
            Mki = M(cols,:);
            U(i,:) = ((Mki'*Mki)+lambda*eye(k))\(Mki'*rki');
        end
    end

    % find RMSE and load resultMatrix1
    PredictedRatings1 = U*M';
    RMSE = sqrt(sum(sum((PredictedRatings1(testIdx)-Ratings(testIdx)).^2))/length(testIdx));
    resultMatrix1(lambdaSet==lambda,1) = RMSE

end

toPlot = zeros(numLambdas,1);

for bLambda = 1:numLambdas
    toPlot1(bLambda) = mean(resultMatrix1(bLambda,:));
end

bestLambda1 = min(toPlot);

