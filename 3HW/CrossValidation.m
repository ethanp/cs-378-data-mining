% Ethan Petuchowski     Data Mining HW#3

% ==============================================================
% ================== INITIALIZATIONS ===========================

load('hw3_netflix.mat')
tau = 30;
k = 10;
lambdaSet = 0.05:0.05:1;
numLambdas = length(lambdaSet)
numValidations = length(cvSet(:,1));
resultMatrix = zeros(numLambdas,numValidations);

% ==============================================================
% ================= RUN EM and PRODUCE RMSEs ===================

% loop through the possible values of lambda
for lambda = lambdaSet
    % find RMSE over the validation sets
    for valNum = 1:numValidations
        U = unidrnd(5, length(Ratings(:,1)), k);    % generate random U,M matrices
        M = unidrnd(5, length(Ratings(1,:)), k);
        trnSet = trR;
        trnSet(cvSet(valNum,:)) = 0;  % zero out test column from training set
        valSet = cvSet(valNum,:);   % make that the test set

        %%%% THE EM %%%%
        for iteration = 1:tau
            % update M
            for j = 1:length(M(:,1))
                % find U(Kj) & r_j(Kj), they correspond to nonzero ROWS of trnSet(:,j)
                [rows,cols,rkj] = find(trnSet(:,j));
                Ukj = U(rows,:);
                M(j,:) = ((Ukj'*Ukj)+lambda*eye(k))\(Ukj'*rkj);
            end

            % update N
            for i = 1:length(U(:,1))
                [rows,cols,rki] = find(trnSet(i,:));
                Mki = M(cols,:);
                U(i,:) = ((Mki'*Mki)+lambda*eye(k))\(Mki'*rki');
            end
        end

        % find RMSE and load resultMatrix
        PredictedRatings = U*M';
        RMSE = sqrt(sum(sum((PredictedRatings(valSet)-trR(valSet)).^2))/length(valSet));
        resultMatrix(lambdaSet==lambda,valNum) = RMSE

    end
end

toPlot = zeros(numLambdas,1);

for bLambda = 1:numLambdas
    toPlot(bLambda) = mean(resultMatrix(bLambda,:))
end

bestLambda1 = min(toPlot)

