On MATLAB console, type:

        load Netflix.mat

This will load following matrices: 
        Ratings (sparse format),    -- known ratings
        trR (sparse format),        -- TrainingMatrix
        trIdx,                      -- indices of Ratings used to make TrainingMatrix
        testIdx,                    -- indices of Ratings NOT used to make TrainingMatrix
        cvSet.                      -- indices for each of 10 cross-validation sets

Ratings:
    Matrix of all known ratings for 1978 users and 4635 movies. 
    An element-value of 0 implies an unknown rating for that element. 

trR:
    Matrix same size as Ratings matrix but some of the entries have been set to be 0.
    This matrix is to be used as the Training matrix, 
        where entries corresponding to the Test data has been set to be zero or unknown. 

trIdx:
    Indices of Ratings matrix which we use as training data, 
    i.e. trR=Ratings(trIdx).

testIdx:
    Indices of Ratings matrix which we've set to 0. 
    
    Once you calculate U and M, calculate PredictedRatings=U*M^T.
    Now your RMSE will be 
        sqrt(sum(sum((PredictedRatings(testIdx)-Ratings(testIdx)).^2))/length(testIdx)),
            i.e. you take mean squared error of your predictions for indices corresponding to testIdx 
                with the known ratings for testIdx stored in matrix Ratings. 

cvSet:
    Matrix that contains indices for 10-fold Cross-validation set. 
    Row cvSet(1, :) contains the indices for first cross-validation set. 
    Thus, if you want to leave the first set out and only train on sets 2-10, you should type:

        trR1=trR;
        trR1(cvSet(1,:))=0;

    i.e. you set elements of your training data corresponding to the first set to be unknown.
    Now you train using trR1 data and after you find out the solution matrix U1 and M1, 
        calculate RMSE using:
            sqrt(sum(sum((PredictedRatings1(cvSet(1,:))-trR(cvSet(1,:))).^2))/length(cvSet(1,:))).


Useful commands:
    ===========================
    NONZEROS:
        s = nonzeros(A)
        returns a full column vector of the nonzero elements in A, ordered by columns.
            ( i.e. gives the s, but not the i and j, from [i,j,s] = find(A) )

    ===========================
    FIND:
        ind = find(X) locates all nonzero elements of array X, and 
            returns the linear indices of those elements in vector ind. 
        If X is a row vector, then ind is a row vector; 
            otherwise, ind is a column vector. 
        If X contains no nonzero elements or is an empty array, 
            then ind is an empty array.

        [i,j,s] = find(A) will produce:  *** THIS IS THE GOOD ONE ***
            i = vector of rows in which the nonzeros were found
            j = vector of columns in which the nonzeros were found
            d = vector of the values of the nonzeros
    
    ===========================
    ARITHMETIC:
        A' == transpose(A)

        A * B  => normal Matrix multiplication
        
        A .* B => element-by-element product, i.e. Cij = Aij * Bij


