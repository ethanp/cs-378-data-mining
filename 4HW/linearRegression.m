% Data Mining HW#4: Regression      4/13/13

% add in the constant feature
constTrain = ones(length(Xtrain(:,1)),1);
constTest = ones(length(Xtest(:,1)),1);
XtoTrain = [constTrain Xtrain];
XtoTest = [constTest Xtest];


% use the Normal equation to find the weights-vector
weightsVec = (XtoTrain'*XtoTrain)\XtoTrain'*ytrain

% predict values of ytest
yPredic = XtoTest*weightsVec;

% find statistics
errors = ytest - yPredic;
sqrErrors = errors.^2;
sumSqrError = sum(sqrErrors);
meanSqrError = sumSqrError/length(ytest)
rootMeanSqrError = sqrt(meanSqrError)

